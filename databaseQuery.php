<?php
function db(){
    $host = 'localhost';
    $database = 'mangashop';
    $username = 'root';
    $password = '';
    

    try {
        $connection = new PDO('mysql:host='.$host.';dbname='.$database, $username, $password);
        $connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $connection;
    }
    catch(PDOException $e) {
        dd($e->getMessage());
    }
}


// SELECT QUERY
// verbinding maken met database
$connection = db();

// Query draaien: 'SELECT * FROM products'
try {
	$products = $connection->prepare('SELECT * FROM products');
	$products->execute([]);
	$products->setFetchMode(PDO::FETCH_ASSOC);
	$products = $products->fetchAll();
}
catch(PDOException $e) {
	print_r($e->getMessage());
	die();
}

// Data outputten
print_r($products);

// INSERT QUERY
// verbinding maken met database
// Query draaien

// UPDATE QUERY
// verbinding maken met database
// Query draaien

// DELETE QUERY
// verbinding maken met database
// Query draaien

